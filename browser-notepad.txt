data:text/html;charset=utf-8,
<title>Browser Notepad</title>
<link rel="shortcut icon" href="http://g.etfv.co/http://www.sublimetext.com"/>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
<style>
	body {
		margin: 0;
		padding: 0;
	}

	textarea {
		background: black;
		color: limegreen;
		font-family: 'Droid Sans Mono',sans-serif;
		font-size: 14px;
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 4em;
		border: none;
		outline: none;
		resize: none;
	}
</style>
<script>
	window.addEventListener('load',function() {
		var contentData = document.getElementById("content");
		contentData.addEventListener('keyup',function(e) {
			var keyCheck = e.keyCode || e.which;
			
			if (keyCheck == 9) {
				contentData.value += "\t";
				window.focus();
				e.preventDefault();
			}
			
			if (contentData.value.length < 20) {
				document.title = contentData.value;
			} else {
				document.title = contentData.value.substr(0,20);
			}
		});
	});
</script>
<body>
<textarea autofocus id="content" spellcheck="false">