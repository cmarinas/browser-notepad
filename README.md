Browser Notepad
===============

Inspired by: https://coderwall.com/p/lhsrcq

Usage: Copy the code into the address bar of any HTML5 supported browser for a browser-based notepad.

Features
--------
- Can be bookmarked
- UTF-8
- Content-based title
- Favicon (currently using the one for Sublime Text)
- Fixed width font (Droid Sans Mono)
- Green text on black background
- Extra whitespace and slightly larger font for easier readability
- Autofocus
- Spellcheck disabled
- Tab detection

Wishlist
--------
- Local storage/persistence
- Line numbering